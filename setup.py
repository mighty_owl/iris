#!/usr/bin/env python

from setuptools import setup

setup(name='Iris',
      version='1.0',
      description='Speech recognition script',
      author='Tamas Deak',
      author_email='thomasckik@gmail.com',
      url='https://gitlab.com/mighty_owl/iris',
      license='',
      packages=[],
      install_requires=['pip',
                        'setuptools',
                        'wheel',
                        'SpeechRecognition',
                        'pytest',
                        # 'pyaudio', rework needed for regression
                        # 'pocketsphinx',  # rework needed, would be better with distutils install from its repo
                        'google-api-python-client'],  # not installed yet
      )
