class Iris:
    def __init__(self):
        print('Iris is listening...')

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __del__(self):
        pass