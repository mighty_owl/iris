from iris_tests import test_base
from unittest import TestCase
from unittest.mock import patch
from iris_modules.iris_sr import Iris


class TestIris(test_base.TestBase, TestCase):
    def setup_class(cls):
        print("TestIris setup method")

    @patch("builtins.print")
    def test_contructor(self, mock_print):
        with Iris() as test_iris:
            mock_print.assert_called_with("Iris is listening...")
